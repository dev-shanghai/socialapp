//
//  AppDelegate.swift
//  SocialApp
//
//  Created by ABBC on 4/20/21.
//
import Foundation
import UIKit

@IBDesignable
class CardView: UIView {
    @IBInspectable var cornerRadius: CGFloat = 10
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 2
    @IBInspectable var shadowClr: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.2
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        layer.masksToBounds = false
        layer.shadowColor = shadowClr?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
    }



}
class RoundedCardView: UIView {
    
    override func layoutSubviews() {
        layer.cornerRadius = 20.0
     //   layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        layer.shadowRadius = 3.0
        layer.shadowOpacity = 0.7
    }
}

