//
//  CommentsViewController.swift
//  SocialApp
//
//  Created by ABBC on 4/22/21.
//

import UIKit

class CommentsViewController: UIViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    var viewModel:CommentViewModel? =  CommentViewModel()
    var  homeDataModel = HomeFeedDataModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel = CommentViewModel()
        viewModel?.delegate = self
        viewModel?.setUpView()
        self.setUpView()
        self.getAllPostComments()
        
    }
    func setUpView() {
        self.tableView.estimatedRowHeight = 400
        self.tableView.estimatedSectionHeaderHeight = 200
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.sectionHeaderHeight = UITableView.automaticDimension
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.viewModel?.delegate = self
        
    }
    func backBtnPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func didTapNavigationBackBtn(_ sender: UIButton) {
        self.backBtnPressed()
    }
    static var storyboardInstance:CommentsViewController{
        let storyboard = R.StoryboardRef.Main
        if #available(iOS 13.0, *) {
            let vc = storyboard.instantiateViewController(identifier: "CommentsViewController") as!CommentsViewController
            return vc
        } else {
            let vc = storyboard.instantiateViewController(withIdentifier: "CommentsViewController") as!CommentsViewController
            return vc
        }
        
    }
    
    
    
}

extension CommentsViewController: CoordinatorProtocol {
    func hideActivityIndicator(message: String) {
        DispatchQueue.main.async {[weak self] in
            self?.activityIndicator.stopAnimating()
        }
    }
    
    func failure(message: String) {
        
    }
    
    func successResponse(data: [HomeFeedDataModel] , pageNo: Int) {
        self.viewModel?.totalPages = pageNo
        self.viewModel?.homeDataModel = homeDataModel
        self.viewModel?.feedsLocalArray.append(contentsOf: data)
        //        self.viewModel?.feedsModel = data
        DispatchQueue.main.async { [weak self] in
            self?.tableView.delegate = self
            self?.tableView.dataSource = self
            self?.tableView.reloadData()
        }
        
    }
}

//MARK:- table view delegates
extension CommentsViewController:UITableViewDelegate,UITableViewDataSource{
    //MARK:- will display
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == ((viewModel?.feedsLocalArray.count)!) - 1 { //
            if let viewModels = viewModel {
                if  viewModels.pageNo  < viewModels.totalPages  {
                    viewModel?.pageNo += 1
                    self.getAllPostComments()
                }
            }
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard viewModel?.feedsLocalArray.count ?? 0>0 else {
            return 0
        }
        return viewModel?.feedsLocalArray.count ?? 0
    }
    //Mark:-header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?   {
         guard let headerCell = tableView.dequeueReusableCell(withIdentifier: "PostDetailsTableViewCell") as? PostDetailsTableViewCell else {
             return UIView()
         }
        headerCell.setData(feedDataObj: viewModel?.homeDataModel)
        return headerCell.contentView
        
     }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentsTableViewCell") as! CommentsTableViewCell
        cell.indexPath = indexPath
        let obj = viewModel?.feedsLocalArray[indexPath.row]
        cell.setData(feedDataObj: obj)
        return cell
        
    }
   
}
