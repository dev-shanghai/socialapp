//
//  PostHeaderTableViewCell.swift
//  SocialApp
//
//  Created by ABBC on 4/22/21.
//

import UIKit

class PostDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var postLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    var indexPath:IndexPath?
    override func awakeFromNib() {
        super.awakeFromNib()
       
        // Initialization code
    }
   
    func setData(feedDataObj:HomeFeedDataModel?)  {
        self.nameLbl.text = feedDataObj?.name
        self.titleLbl.text = feedDataObj?.title
        self.postLbl.text = feedDataObj?.body
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
