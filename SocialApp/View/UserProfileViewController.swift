//
//  UserProfileViewController.swift
//  SocialApp
//
//  Created by ABBC on 4/22/21.
//

import UIKit

class UserProfileViewController: UIViewController {
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var activeStatusImageView: UIImageView!
    @IBOutlet weak var navBackBtn: UIButton!
    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var nameValueLbl: UILabel!
    @IBOutlet weak var emailValueLbl: UILabel!
    @IBOutlet weak var genderValueLbl: UILabel!
    var userId:Int?
    private(set) var viewModel: UserProfileViewModel?
    var userDataResult:UserDetailBaseDataModel?{
        didSet{
            guard let userData =  userDataResult else {return}
            viewModel = UserProfileViewModel.init(model: userData)
            DispatchQueue.main.async {
                self.updateLabels()
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel = UserProfileViewModel()
        self.viewModel?.viewController = self
        self.viewModel?.delegate = self
        viewModel?.setUpView()
        viewModel?.getUserDetails()
    }
    
    @IBAction func didTapNavigationBackBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    func updateLabels() {
        guard  let viewModel = viewModel else {
            return
        }
        self.nameValueLbl.text = viewModel.name
        self.emailValueLbl.text = viewModel.email
        self.genderValueLbl.text = viewModel.gender
    }
    
    static var storyboardInstance:UserProfileViewController{
        let storyboard = R.StoryboardRef.Main
        if #available(iOS 13.0, *) {
            let vc = storyboard.instantiateViewController(identifier: "UserProfileViewController") as!UserProfileViewController
            return vc
        } else {
            let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileViewController") as!UserProfileViewController
            return vc
        }
        
    }
    
    
    
}

extension UserProfileViewController: UserProfileApiResponse {
    func userProfileSuccess(model: UserDetailBaseDataModel) {
        self.userDataResult = model
    
    }
    
    func hideActivityIndicator(message: String) {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
        }
    }
}
