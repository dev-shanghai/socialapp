//
//  CommentViewModel.swift
//  SocialApp
//
//  Created by ABBC on 4/22/21.
//

import UIKit

struct CommentViewModel {
    var pageNo:Int=1
    var limit:Int=20
    var totalPages:Int=1
    var feedsLocalArray:[HomeFeedDataModel] = []
    var homeDataModel = HomeFeedDataModel()
    var delegate: CoordinatorProtocol?

    var feedsModel = HomeFeedBaseModel()
   
    //MARK:- post data
    
    func setUpView()  {
        ApiManager.sharedApiManager.delegate = self
    }
    init(feedModel: HomeFeedBaseModel) {
    }
    init() {
        
    }
   
}


//MARK:-
//MARK:- Get all post comments
extension CommentViewModel:ApiResponseProtocol{
    mutating func success(model: Any?) {
        delegate?.hideActivityIndicator(message: "Check")
        if let model = model as? HomeFeedBaseModel{
            feedsModel.data = model.data.sorted(by: {$0.updatedAt > $1.updatedAt})
            feedsModel = model
            appendDataWithLocalArray(feedsModel: feedsModel)
        }
        
    }
    
    func failure(error: String?) {
        delegate?.hideActivityIndicator(message: "")
        
        
    }
    
    mutating  func appendDataWithLocalArray(feedsModel:HomeFeedBaseModel?) {
        if let model = feedsModel{
            self.totalPages = model.meta.pagination.pages
            if model.data.count>0 {
                print(self.feedsLocalArray.count)
                delegate?.successResponse(data: model.data , pageNo: self.totalPages)
            }
        }
    }
   
    
    
}
extension CommentsViewController{
    func getPostCommentsParams() -> [String:Any] {
        var params:[String:Any] = [:]
        params["page"] = "\(self.viewModel?.pageNo ?? 1)"
        params["limit"] = "\(self.viewModel?.limit ?? 1)"
        return params
    }
    
      func getAllPostComments() {
        self.activityIndicator.startAnimating()
        ApiManager.sharedApiManager.getHomeFeeds(url: URLManager.urlSharedInstance.getPostComments, getPostCommentsParams())
        
    }
    
}
