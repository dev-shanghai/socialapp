//
//  CommentsTableViewCellViewModel.swift
//  SocialApp
//
//  Created by ABBC on 4/22/21.
//

import UIKit

class CommentsTableViewCellViewModel: NSObject {
    weak var cell:CommentsTableViewCell?
    var indexPath:IndexPath?
    func setData(feedDataObj:HomeFeedDataModel?)  {
        self.cell?.nameLbl.text = feedDataObj?.name
        self.cell?.emailLbl.text = feedDataObj?.email
        self.cell?.postLbl.text = feedDataObj?.body
    }
  
}
