//
//  HomeTableViewCellViewModel.swift
//  SocialApp
//
//  Created by ABBC on 4/21/21.
//

import UIKit

class HomeTableViewCellViewModel: NSObject {
    weak var cell:HomeTableViewCell?
    var indexPath:IndexPath?
    weak var delegate:CoordinatorProtocol?
    var didSelectProfile: (_ indexPath: IndexPath)->Void = {_ in}
    var didSelectPost: (_ indexPath: IndexPath)->Void = {_ in}

    func setData(feedDataObj:HomeFeedDataModel?)  {
        self.cell?.titleLbl.text = feedDataObj?.title
        self.cell?.postLbl.text = feedDataObj?.body
    }
    func goToUserProfile() {
        delegate?.goToUserProfile(indexPath: indexPath ?? IndexPath())
    }
    func goToCommentSection() {
        delegate?.goToCommentSection(indexPath: indexPath ?? IndexPath())

    }
    
}
