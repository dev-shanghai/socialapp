//
//  CommentsDetailsHeaderTableViewCellViewModel.swift
//  SocialApp
//
//  Created by ABBC on 4/22/21.
//

import UIKit

class PostDetailsTableViewCellViewModel: NSObject {
    weak var cell:PostDetailsTableViewCell?
    var indexPath:IndexPath?
    func setData(feedDataObj:HomeFeedDataModel?)  {
        self.cell?.nameLbl.text = feedDataObj?.name
        self.cell?.titleLbl.text = feedDataObj?.title
        self.cell?.postLbl.text = feedDataObj?.body
    }
}
