//
//  ApiManager.swift
//  SocialApp
//
//  Created by ABBC on 4/21/21.
//

import UIKit
protocol ApiResponseProtocol  {
   mutating func success(model: Any?)
   mutating func failure(error:String?)
}
class ApiManager:NSObject {
    static let sharedApiManager = ApiManager()
    var delegate:ApiResponseProtocol?
    private override init() {}
    //MARK:- Get Home Feeds
    func getHomeFeeds(url:String,_ params:[String: Any]) {
        BaseApiCaller.sharedBaseApiManager.post(requestType: .get, url:url, parameters: params) { (data, error,statusCode) in
            if let data = data {
                do{
                    let resp = try JSONDecoder().decode(HomeFeedBaseModel.self, from: data)
                    print(String(data:data,encoding:.utf8)!)
                    if statusCode == 201 || statusCode == 200 {
                        self.delegate?.success(model: resp)
                        return
                    }else{
                        self.delegate?.failure(error: error?.localizedDescription)
                        return
                    }
                }catch{
                    self.delegate?.failure(error: error.localizedDescription)
                    return
                }
            }else if let error = error{
                self.delegate?.failure(error: error.localizedDescription)
                return
            }else{
                self.delegate?.failure(error: "error")
                return
            }
        }
    }
    //MARK:Get user details-
    
    func getUserDetails(url:String?,_ params:[String: Any]) {
        guard let url = url else {
            return
        }
        BaseApiCaller.sharedBaseApiManager.post(requestType: .get, url: url, parameters: params) { (data, error,statusCode) in
            if let data = data {
                do{
                    let resp = try JSONDecoder().decode(UserDetailBaseDataModel.self, from: data)
                    print(String(data:data,encoding:.utf8)!)
                    if statusCode == 201 || statusCode == 200 {
                        self.delegate?.success(model: resp)
                        return
                    }else{
                        self.delegate?.failure(error: error?.localizedDescription)
                        return
                    }
                }catch{
                    self.delegate?.failure(error: error.localizedDescription)
                    return
                }
            }else if let error = error{
                self.delegate?.failure(error: error.localizedDescription)

                return
            }else{
                self.delegate?.failure(error: "error")
                return
            }

        }
    }
}
